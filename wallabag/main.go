package wallabag

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type SyncStrategy uint8

const (
	NoSync SyncStrategy = iota
	Push
	Pull
	Merge
)

type Wallabag struct {
	Config WallabagConfig
	Token  WallabagToken
	Items  []WallabagItem
}

type WallabagConfig struct {
	GrantType       string       `json:"grant_type"`
	Host            string       `json:"host"`
	ClientId        string       `json:"client_id"`
	ClientSecret    string       `json:"client_secret"`
	Username        string       `json:"username"`
	Password        string       `json:"password"`
	ArchiveStrategy SyncStrategy `json:"-"`
	TagStrategy     SyncStrategy `json:"-"`
}

type WallabagToken struct {
	AccessToken  string `json:"access_token"`
	ExpiresIn    int64  `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
	TokenType    string `json:"token_type"`
}

type WallabagPage struct {
	Page     int64         `json:"page"`
	Limit    int64         `json:"limit"`
	Pages    int64         `json:"pages"`
	Total    int64         `json:"total"`
	Embedded WallabagItems `json:"_embedded"`
}

type WallabagItems struct {
	Items []WallabagItem `json:"items"`
}

type WallabagItem struct {
	Id         int64         `json:"id"`
	Title      string        `json:"title"`
	CreatedAt  string        `json:"created_at"`
	UpdatedAt  string        `json:"updated_at"`
	IsStarred  bool          `json:"is_starred"`
	IsArchived bool          `json:is_archived"`
	Content    string        `json:"content"`
	Tags       []WallabagTag `json:"tags"`
}

type WallabagTag struct {
	Id    int64  `json:"id"`
	Label string `json:"label"`
}

func New(host string, ID string, secret string, username string, password string, tagStrategy SyncStrategy, archiveStrategy SyncStrategy) Wallabag {
	var wallabag Wallabag
	var config WallabagConfig
	config.GrantType = "password"
	config.Host = host
	config.ClientId = ID
	config.ClientSecret = secret
	config.Username = username
	config.Password = password
	config.TagStrategy = tagStrategy
	config.ArchiveStrategy = archiveStrategy
	wallabag.Config = config
	wallabag.login()
	wallabag.getPages(1)
	return wallabag
}

func ParseSyncStrategy(strategy string) (*SyncStrategy, error) {
	var ret SyncStrategy

	switch strategy {
		case "":
		case "none":
			ret = NoSync
		case "push":
			ret = Push
		case "pull":
			ret = Pull
		case "merge":
			ret = Merge
		default:
			return nil, errors.New("Unrecognized sync strategy: " + strategy)
	}

	return &ret, nil
}

func (w *Wallabag) setArchived(entry WallabagItem, archive string) {
	client := &http.Client{}

	form := url.Values{}
	form.Add("archive", archive)
	req_data, _ := json.Marshal(form)

	id := strconv.FormatInt(entry.Id, 10)
	req, _ := http.NewRequest("PATCH", "https://"+w.Config.Host+"/api/entries/"+id+".json", bytes.NewReader(req_data))
	req.Header.Set("Authorization", "Bearer "+w.Token.AccessToken)
	req.Header.Set("Content-Type", "application/json")

	client.Do(req)
}

func (w *Wallabag) Archive(entry WallabagItem) {
	w.setArchived(entry, "1")
}

func (w *Wallabag) Unarchive(entry WallabagItem) {
	w.setArchived(entry, "0")
}

func (w *Wallabag) AddTags(entry WallabagItem, tags []string) {
	client := &http.Client{}

	form := url.Values{}
	form.Add("tags", strings.Join(tags, ","))
	req_data, _ := json.Marshal(form)

	id := strconv.FormatInt(entry.Id, 10)
	req, _ := http.NewRequest("POST", "https://"+w.Config.Host+"/api/entries/"+id+"/tags.json", bytes.NewReader(req_data))
	req.Header.Set("Authorization", "Bearer "+w.Token.AccessToken)
	req.Header.Set("Content-Type", "application/json")

	client.Do(req)
}

func (w *Wallabag) DeleteTag(entry WallabagItem, tag WallabagTag) {
	client := &http.Client{}

	entry_id := strconv.FormatInt(entry.Id, 10)
	tag_id := strconv.FormatInt(tag.Id, 10)

	req, _ := http.NewRequest("DELETE", "https://"+w.Config.Host+"/api/entries/"+entry_id+"/tags/"+tag_id+".json", nil)
	req.Header.Set("Authorization", "Bearer "+w.Token.AccessToken)
	req.Header.Set("Content-Type", "application/json")

	client.Do(req)
}

func (w *Wallabag) login() {
	req, _ := json.Marshal(w.Config)
	resp, _ := http.Post("https://"+w.Config.Host+"/oauth/v2/token", "application/json", bytes.NewBuffer(req))
	body, _ := ioutil.ReadAll(resp.Body)
	var token WallabagToken
	json.Unmarshal(body, &token)
	w.Token = token
}

func (w *Wallabag) getPages(p int64) {
	page := strconv.FormatInt(p, 10)

	client := &http.Client{}

	req, _ := http.NewRequest("GET", "https://"+w.Config.Host+"/api/entries.json", nil)
	req.Header.Set("Authorization", "Bearer "+w.Token.AccessToken)
	q := req.URL.Query()
	q.Add("detail", "full")
	q.Add("page", page)

	// Don't bother retrieving archived items if we only want to push.
	if w.Config.ArchiveStrategy == Push {
		q.Add("archive", "0")
	}

	req.URL.RawQuery = q.Encode()
	res, _ := client.Do(req)
	body, _ := ioutil.ReadAll(res.Body)

	var wallabagPage WallabagPage
	json.Unmarshal(body, &wallabagPage)

	if wallabagPage.Page != wallabagPage.Pages {
		w.getPages(wallabagPage.Page + 1)
	}
	w.Items = append(w.Items, wallabagPage.Embedded.Items...)
}
