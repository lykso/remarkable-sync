.PHONY: clean

remarkable-sync:
	env GOOS=linux GOARCH=arm GOARM=7 go build -o remarkable-sync

clean:
	rm -f remarkable-sync
