package cmd

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/vlaborie/remarkable-sync/remarkable"
	"gitlab.com/vlaborie/remarkable-sync/wallabag"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	wallabagConfig *viper.Viper

	wallabagCmd = &cobra.Command{
		Use:   "wallabag",
		Short: "Sync with Wallabag service",
		Long:  `Sync with Wallabag service.`,
		Run: func(cmd *cobra.Command, args []string) {
			Remarkable := remarkable.New("/home/root/.local/share/remarkable/xochitl/")

			if err := wallabagConfig.ReadInConfig(); err == nil {
				fmt.Println("Enable Wallabag sync with config file:", wallabagConfig.ConfigFileUsed())
				Remarkable.Items = append(Remarkable.Items, Remarkable.AddDir("wallabag", "Wallabag", ""), Remarkable.AddDir("wallabag-archived", "Archived", "wallabag"))

				tagStrategyPtr, err := (wallabag.ParseSyncStrategy(wallabagConfig.GetString("tag_strategy")))

				if err != nil {
					fmt.Errorf(err.Error())
					return
				}

				archiveStrategyPtr, err := (wallabag.ParseSyncStrategy(wallabagConfig.GetString("archive_strategy")))

				if err != nil {
					fmt.Errorf(err.Error())
					return
				}

				Wallabag := wallabag.New(wallabagConfig.GetString("host"), wallabagConfig.GetString("client_id"), wallabagConfig.GetString("client_secret"), wallabagConfig.GetString("username"), wallabagConfig.GetString("password"), *tagStrategyPtr, *archiveStrategyPtr)

				for _, WallabagItem := range Wallabag.Items {
					var RemarkableItem remarkable.RemarkableItem
					RemarkableItemPtr, _ := Remarkable.LoadEpub(strconv.FormatInt(WallabagItem.Id, 10))
					// For readability
					file_exists := (RemarkableItemPtr != nil)

					if file_exists {
						RemarkableItem = *RemarkableItemPtr

					} else {
						RemarkableItemContent := remarkable.RemarkableItemContent{
							Content: []byte(WallabagItem.Content),
						}

						RemarkableItem = remarkable.RemarkableItem{
							Id:               strconv.FormatInt(WallabagItem.Id, 10),
							Type:             "DocumentType",
							Parent:           "wallabag",
							VisibleName:      WallabagItem.Title,
							LastModified:     WallabagItem.UpdatedAt,
							Version:          1,
							Deleted:          false,
							MetadataModified: false,
							Modified:         false,
							Pinned:           WallabagItem.IsStarred,
							Synced:           false,
							ContentType:      "html",
							Content:          RemarkableItemContent,
						}
					}

					// Begin archive syncing
					archiveSync:switch Wallabag.Config.ArchiveStrategy {
						case wallabag.NoSync:
							break

						case wallabag.Push:
							if !file_exists {
								break archiveSync
							}

							if RemarkableItem.Parent == "wallabag" && WallabagItem.IsArchived {
								Wallabag.Unarchive(WallabagItem)
							} else if RemarkableItem.Parent == "wallabag-archived" && !WallabagItem.IsArchived {
								Wallabag.Archive(WallabagItem)
							}

						case wallabag.Pull:
							if WallabagItem.IsArchived {
								RemarkableItem.Parent = "wallabag-archived"
							} else {
								RemarkableItem.Parent = "wallabag"
							}

						case wallabag.Merge:
							if WallabagItem.IsArchived {
								RemarkableItem.Parent = "wallabag-archived"

							} else if RemarkableItem.Parent == "wallabag-archived" && !WallabagItem.IsArchived {
								Wallabag.Archive(WallabagItem)
							}

					} // End archive syncing

					// Begin tag syncing
					tagSync:switch Wallabag.Config.TagStrategy {
						case wallabag.NoSync:
							break tagSync

						case wallabag.Push:
							remarkable_tags := make(map[string]bool)
							remarkable_tags_slice := make([]string, len(RemarkableItem.Content.Tags))

							for i, tag := range RemarkableItem.Content.Tags {
								remarkable_tags[tag.Name] = true
								remarkable_tags_slice[i] = tag.Name
							}
							Wallabag.AddTags(WallabagItem, remarkable_tags_slice)

							wallabag_tags := make(map[string]bool)
							for _, tag := range WallabagItem.Tags {
								wallabag_tags[tag.Label] = true

								if !remarkable_tags[tag.Label] {
									Wallabag.DeleteTag(WallabagItem, tag)
								}
							}

						case wallabag.Pull:
							timestamp := time.Now().Unix()
							RemarkableItem.Content.Tags = make([]remarkable.RemarkableTag, len(WallabagItem.Tags))

							for i, tag := range WallabagItem.Tags {
								RemarkableItem.Content.Tags[i] = remarkable.RemarkableTag{tag.Label, timestamp}
							}

						case wallabag.Merge:
							remarkable_tags_slice := make([]string, len(RemarkableItem.Content.Tags))

							for i, tag := range RemarkableItem.Content.Tags {
								remarkable_tags_slice[i] = tag.Name
							}
							Wallabag.AddTags(WallabagItem, remarkable_tags_slice)

							for _, tag := range WallabagItem.Tags {
								RemarkableItem.Content.AddTag(tag.Label)
							}
					} // End tag syncing.

					Remarkable.Items = append(Remarkable.Items, RemarkableItem)
				}
			}

			Remarkable.Write()
		},
	}
)

func init() {
	rootCmd.AddCommand(wallabagCmd)
	cobra.OnInitialize(initWallabagConfig)
}

func initWallabagConfig() {
	home, err := homedir.Dir()
	if err != nil {
		panic(err)
	}

	wallabagConfig = viper.New()
	wallabagConfig.SetDefault("host", "app.wallabag.it")
	wallabagConfig.AddConfigPath("/etc/remarkable-sync")
	wallabagConfig.AddConfigPath(home + "/.config/remarkable-sync")
	wallabagConfig.AddConfigPath("./config")
	wallabagConfig.SetConfigName("wallabag")
}
