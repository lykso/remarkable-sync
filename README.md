# remarkable-sync

**remarkable-sync** is a Golang tool design to run on reMarkable paper tablet for syncing your document from different services.

The main repository is hosted on [Gitlab](https://gitlab.com/vlaborie/remarkable-sync).

## Supported services

* [Wallabag](https://www.wallabag.org)
* [Miniflux](https://miniflux.app)

## Build

Build for run on reMarkable tablet :

~~~
env GOOS=linux GOARCH=arm GOARM=7 go build -o remarkable-sync
~~~

## Connection

After configuring Wifi on your reMarkable paper tablet, you can access to it with SSH:

~~~
ssh root@192.168.X.X
~~~

*// Password and IP address are indicated in the end of **About** menu in reMarkable settings !*

## Install

For install, you just need to upload **remarkable-sync** to your reMarkable:

~~~
ssh root@192.168.X.X 'mkdir -p /usr/local/bin'
scp remarkable-sync root@192.168.X.X:/usr/local/bin
~~~

## Config

### Wallabag

Edit `/etc/remarkable-sync/wallabag.yaml` on your reMarkable:

~~~
host: "app.wallabag.it"
client_id: "client_id"
client_secret: ""client_secret
username: "login"
password: "password"
tag_strategy: "none|push|pull|merge"
archive_strategy: "none|push|pull|merge"
~~~

`tag_strategy` and `archive_strategy` determine how synchronization of article
tags and archived articles is to be done.

- "none" results in no synchronization of these things at all.
- "push" for tags results in tags on unarchived articles in Wallabag being set
  to whatever they are on the reMarkable. For the archive, it results in
  articles in the "Archived" folder being archived in Wallabag. Tags are synced
  before the article is archived.
- "pull" for tags results in all tags on an article being set to whatever they
  are in Wallabag. For the archive, it results in the "Archived" folder being
  filled only with articles archived in Wallabag.
- "merge" for tags results in all tags on both archived and unarchived articles
  on both the reMarkable and in Wallabag being set to the union of the tags in
  both. For the archive, it results in any articles added to the "Archived"
  folder getting archived in Wallabag, and all archived articles in Wallabag
  being downloaded into the "Archived" folder.

The default strategy, if none is set, is "none."

### Miniflux

Edit `/etc/remarkable-sync/miniflux.yaml` on your reMarkable:

~~~
host: "reader.miniflux.app"
token: "token"
~~~

## Usage

Connect to the tablet, run **remarkable-sync**, then restart **xochitl**:

~~~
ssh root@192.168.X.X
remarkable-sync [wallabag|miniflux]
systemctl restart xochitl
~~~
